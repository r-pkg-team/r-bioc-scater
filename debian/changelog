r-bioc-scater (1.34.1+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.2 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Mon, 10 Mar 2025 10:32:54 +0900

r-bioc-scater (1.34.0+ds-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 21:31:01 +0100

r-bioc-scater (1.34.0+ds-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Nov 2024 14:34:58 +0100

r-bioc-scater (1.32.1+ds-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:14:18 +0200

r-bioc-scater (1.32.1+ds-1~0exp1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 30 Jul 2024 16:15:19 +0200

r-bioc-scater (1.32.0+ds-1~0exp) experimental; urgency=low

  * Team upload.
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 10 Jul 2024 11:05:10 +0200

r-bioc-scater (1.30.1+ds-2) unstable; urgency=medium

  * Team upload.
  * Run test only for amd64 and arm64

 -- Andreas Tille <tille@debian.org>  Tue, 19 Dec 2023 09:01:51 +0100

r-bioc-scater (1.30.1+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 08:39:27 +0100

r-bioc-scater (1.28.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 06 Aug 2023 23:33:14 +0200

r-bioc-scater (1.26.1+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Reduce piuparts noise in transitions (routine-update)
  * Build-Depends: r-cran-ggrastr
    Closes: #1024751

 -- Andreas Tille <tille@debian.org>  Mon, 28 Nov 2022 08:22:52 +0100

r-bioc-scater (1.24.0+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Build-Depends: r-cran-rcppml
  * Ignore test requiring not yet packaged r-bioc-snifter
    Closes: #1011412

 -- Andreas Tille <tille@debian.org>  Tue, 31 May 2022 14:56:07 +0200

r-bioc-scater (1.22.0+ds-1) unstable; urgency=medium

  * Team upload.
  * Disable reprotest
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 22:13:33 +0100

r-bioc-scater (1.20.1+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R
  * Provide autopkgtest-pkg-r.conf to make sure Test-Depends will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database.

 -- Andreas Tille <tille@debian.org>  Sun, 12 Sep 2021 21:54:45 +0200

r-bioc-scater (1.18.3+ds-4) unstable; urgency=medium

  * Team upload
  * Skip Testsuite: autopkgtest-pkg-r
    Closes: #982115

 -- Andreas Tille <tille@debian.org>  Sun, 07 Feb 2021 11:34:46 +0100

r-bioc-scater (1.18.3+ds-3) unstable; urgency=medium

  * Team upload.
  * Ignore some failing tests on i386
  * Standards-Version: 4.5.1 (routine-update)
  * Architecture: all

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jan 2021 15:50:02 +0100

r-bioc-scater (1.18.3+ds-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Restore build-dep on r-bioc-scuttle, r-bioc-delayedmatrixstats

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 22 Nov 2020 12:16:14 +0100

r-bioc-scater (1.18.3+ds-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version

  [ Michael R. Crusoe ]
  * debian/control: build-dep on r-bioc-scuttle

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 22 Nov 2020 10:44:06 +0100

r-bioc-scater (1.16.2+ds-2) unstable; urgency=medium

  * Team upload.
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 28 Oct 2020 11:37:07 +0100

r-bioc-scater (1.16.2+ds-1) unstable; urgency=medium

  * Initial release (closes: #969060)

 -- Steffen Moeller <moeller@debian.org>  Wed, 26 Aug 2020 16:52:49 +0200
